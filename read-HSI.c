#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define NROWS  5
#define NCOLS  5
#define NBANDS 5

void print_bin(unsigned char value)
{
    for (int i = sizeof(char) * 7; i != -1; i--)
        printf("%d", (value & (1 << i)) >> i );
    //putc('\n', stdout);
}


uint8_t read_file_byte(FILE *fp){

  uint8_t data;
  fread(&data, 1, 1, fp);
  return data;

}

int main() {

  int y,x,z;
  FILE *fp = fopen("compr.raw", "rb");
  unsigned short sample, neighbour;
  unsigned short raw_image[NBANDS][NROWS][NCOLS] = { 0x0 };

  // Used for read a number of bytes from file   
  for(int i=0 ; i<132 ; i++){
        sample = read_file_byte(fp);
        print_bin(sample);
  }

  // Used to read the binary, display and save the values in a image format
  for(x = 0; x < NROWS; x ++){
    for(y = 0; y < NCOLS; y++){
      for(z = 0; z < NBANDS; z++){
        sample = read_file_byte(fp)<<8;
        sample += read_file_byte(fp);
        raw_image[z][y][x] = sample;
        printf("%u ", sample);
      }
    }
  }

  fclose(fp);

  return 0;
}
